Source: freecell-solver
Section: devel
Priority: optional
Maintainer: Gergely Risko <risko@debian.org>
Vcs-Browser: https://salsa.debian.org/debian/freecell-solver
Vcs-Git: https://salsa.debian.org/debian/freecell-solver.git/
Build-Depends:
 debhelper-compat (= 13),
 cmake (>= 2.6),
 gperf,
 libpath-tiny-perl,
 libtemplate-perl,
 python3,
Homepage: http://fc-solve.shlomifish.org/
Standards-Version: 4.1.1
X-Style: black

Package: libfreecell-solver0
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Library for solving Freecell games
 Freecell Solver is a library for automatically solving boards of Freecell and
 similar variants of card Solitaire. This package contains the header files and
 static libraries necessary for developing programs using Freecell Solver.

Package: libfreecell-solver-dev
Architecture: any
Section: libdevel
Depends:
 libfreecell-solver0 (= ${binary:Version}),
 ${misc:Depends},
Description: Library for solving Freecell games (Development files)
 Freecell Solver is a library for automatically solving boards of Freecell and
 similar variants of card Solitaire. This package contains the header files and
 static libraries necessary for developing programs using Freecell Solver.
 .
 This package contains the files necessary to produce binaries which are
 linked against libfreecell-solver.

Package: freecell-solver-bin
Architecture: any
Section: games
Depends:
 python3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Library for solving Freecell games - utilities
 Freecell Solver is a library for automatically solving boards of Freecell and
 similar variants of card Solitaire. This package contains the header files and
 static libraries necessary for developing programs using Freecell Solver.
 .
 This package contains the binaries included with freecell-solver
